<?php

/**
 * @file
 * Allows a user to specify that a taxonomy field should use the
 * autocomplete text field and not the drop down/select boxes
 */

/**
 * Implementation of hook_form_alter().
 */
function taxiselect_form_alter($form_id, &$form) {
  // Is it a node form?
  if (isset($form['type']) && $form['type']['#value'] .'_node_form' == $form_id) {
    // Does it have a taxonomy associated?
    if (isset($form['taxonomy']) && count($form['taxonomy'])) {
      // Is this type flagged for the taxiselect?
      $types = array_filter(variable_get('taxiselect_node_types', array()));
      if (in_array($form['type']['#value'], $types)) {
        $vocabularies = array_filter(variable_get('taxiselect_vocabularies', array()));
        foreach ($form['taxonomy'] as $vid => $vocabulary) {
          if (!is_numeric($vid) || !in_array($vid, $vocabularies)) {
            continue;
          }
          // replace single select vocabs with our own autocomplete field
          if ($vocabulary['#type'] == 'select' && $vocabulary['#multiple'] == 0 && $vocabulary['#size'] == 0) {
            $default_value = '';
            if (count($vocabulary['#default_value'])) {
              $tid = array_shift($vocabulary['#default_value']);
              $parents = taxiselect_parent_path($tid);
              if (count($parents)) {
                $default_value = implode(', ', $parents);
              }
            }
            $form['taxonomy']['taxiselect_'. $vid] = array(
              '#type' => 'textfield',
              '#title' => $vocabulary['#title'],
              '#description' => $vocabulary['#description'],
              '#required' => $vocabulary['#required'],
              '#default_value' => $default_value,
              '#autocomplete_path' => 'taxiselect/autocomplete/'. $vid,
              '#weight' => $vocabulary['#weight'],
              '#maxlength' => 512,
              '#validate' => array('taxiselect_element_validate' => array($vid)),
            );
            unset($form['taxonomy'][$vid]);
          }
        }
      }
    }
  }
}

/**
 * Validates the user data. Must be an exact match.
 */
function taxiselect_element_validate($form, $vid) {
  if (!empty($form['#value'])) {
    $search_terms = explode(',', $form['#value']);
    $search_terms = array_filter(array_map('trim', $search_terms));
    $limit = (int) variable_get('taxiselect_suggestions_limit', 3);
    $items = taxiselect_match_exact_path($search_terms, $vid, max(array(2, $limit)), FALSE, TRUE);
    if (empty($items)) {
      $message = t(variable_get('taxiselect_message_not_found_'. $vid, ''));
      // The string did not match any terms. Add a list of possible terms if required / posssible.
      // Eg: The tag [suggestions] is present.
      if (strpos($message, '[suggestions]')) {
        $matches = array();
        foreach (taxiselect_suggestions($vid, $form['#value'], $limit) as $item) {
          $matches[] = implode(", ", $item);
        }
        $message = str_replace('[suggestions]', theme('item_list', $matches), $message);
      }


      form_error($form, empty($message) ? _taxiselect_message('not found') : $message);
    }
    elseif (count($items) > 1) {
      $message = t(variable_get('taxiselect_message_multiple_matches_'. $vid, ''));
      // The string matched more than one term. Add a list of possible terms if required.
      // Eg: The tag [suggestions] is present.
      if (strpos($message, '[suggestions]')) {
        $matches = array();
        foreach ($items as $item) {
          $matches[] = implode(", ", $item);
        }
        $message = str_replace('[suggestions]', theme('item_list', $matches), $message);
      }
      form_error($form, empty($message) ? _taxiselect_message('multiple') : $message);
    }
  }
}

/**
 * Implementation of hook_menu().
 */
function taxiselect_menu($maycache) {
  $items = array();
  if (!$maycache) {
    $items[] = array(
      'path' => 'admin/settings/taxiselect',
      'title' => t('Taxiselect settings'),
      'description' => t('Change the number of terms returned and trigger points.'),
      'callback' => 'drupal_get_form',
      'callback arguments' => array('taxiselect_admin_settings'),
      'access' => user_access('administer site configuration')
    );
    $items[] = array(
      'path' => 'taxiselect/autocomplete',
      'title' => t('Taxiselect autocomplete'),
      'callback' => 'taxiselect_autocomplete',
      'access' => TRUE, //user_access('create node content'),
      'type' => MENU_CALLBACK
    );
  }
  return $items;
}

/**
 * Implementation of hook_nodeapi().
 */
function taxiselect_nodeapi(&$node, $op, $three, $four) {
  switch ($op) {
    case 'submit':
      if (property_exists($node, 'taxonomy') && is_array($node->taxonomy)) {
        foreach ($node->taxonomy as $vocabulary => $term_path) {
          list($indicator, $vid) = explode('_', $vocabulary);
          if ($indicator == 'taxiselect' && is_numeric($vid)) {
            $tid = '';
            if (!empty($term_path)) {
              $search_terms = explode(',', $term_path);
              $items = taxiselect_match_exact_path(array_filter(array_map('trim', $search_terms)), $vid, 1, FALSE, TRUE);
              if ($items) {
                $tid = key($items[0]);
              }
            }
            $node->taxonomy[$vid] = (string) $tid;
            unset($node->taxonomy[$vocabulary]);
          }
        }
      }
      break;
  }
}

/**
 * Settings form
 */
function taxiselect_admin_settings() {
  $form['taxiselect_suggestions_limit'] = array(
    '#type' => 'select',
    '#title' => t('Taxiselect suggestion limit'),
    '#default_value' => variable_get('taxiselect_suggestions_limit', 3),
    '#options' => drupal_map_assoc(array(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 15, 20, 25)),
    '#description' => t('Shows a number of suggestions if the term matches multiple values or is not found on form submission. You must use the string [suggestions] in the forms error message strings for these to show.'),
    '#required' => TRUE,
  );
  $form['taxiselect_trigger_length'] = array(
    '#type' => 'select',
    '#title' => t('Taxiselect trigger length'),
    '#default_value' => variable_get('taxiselect_trigger_length', 3),
    '#options' => drupal_map_assoc(array(1, 2, 3, 4, 5, 6, 7, 8, 9, 10)),
    '#description' => t('Minimum number of characters before returning an AJAX response. Useful to reduce server load.'),
    '#required' => TRUE,
  );
  $form['taxiselect_response_limit'] = array(
    '#type' => 'select',
    '#title' => t('Taxiselect response limit'),
    '#default_value' => variable_get('taxiselect_response_limit', 10),
    '#options' => drupal_map_assoc(array(1, 2, 3, 4, 5, 10, 15, 20, 25, 30, 40, 50, 75, 100, 200, 250, 500)),
    '#description' => t('Maximum number of results to return per AJAX response. Useful to increase response times/reduce server load or to offer more choices to the user.'),
    '#required' => TRUE,
  );
  $form['taxiselect_node_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Content types to associate this widget with'),
    '#default_value' => array_filter(variable_get('taxiselect_node_types', array())),
    '#options' => array_map('check_plain', node_get_types('names')),
    '#description' => t('A list of node content types you want to use the taxiselect element with.'),
  );
  $form['taxiselect_skip_root'] = array(
    '#type' => 'checkbox',
    '#title' => t('Hide top level categories.'),
    '#default_value' => variable_get('taxiselect_skip_root', 0),
    '#description' => t('If enabled, the root level will be ignored. For example, say the top level is continents, such as Asia, and the second level is countries, this option will hide the continent terms. This restricts selection of the root terms.'),
  );
  $vocabularies = array();
  $v = taxonomy_get_vocabularies();
  foreach ($v as $vocabulary) {
    $vocabularies[$vocabulary->vid] = check_plain($vocabulary->name);
  }
  $form['taxiselect_vocabularies'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Vocabularies to associate this widget with'),
    '#default_value' => array_filter(variable_get('taxiselect_vocabularies', array())),
    '#options' => $vocabularies,
    '#description' => t('A list of vocabularies you want to use the taxiselect element with. Remember that if the taxonomy is not rendered as a single select list, it can not be replaced by the taxiselect widget.'),
  );

  $form['multiple_matches'] = array(
    '#type' => 'fieldset',
    '#title' => t('Multiple term matches found message'),
    '#description' => t('<p>This allows you to override the custom error message for each vocabulary. The standard error message is:</p>')
      .'<ul><li>'. _taxiselect_message('multiple') .'</li><ul>',
  );

  $form['not_found'] = array(
    '#type' => 'fieldset',
    '#title' => t('Term not found message'),
    '#collapsible' => TRUE,
    '#description' => t('<p>This allows you to override the custom error message for each vocabulary. The standard error message is:</p>')
      .'<ul><li>'. _taxiselect_message('not found') .'</li><ul>',
  );

  // custom error messages
  foreach ($v as $vocabulary) {
    $form['not_found']['taxiselect_message_not_found_'. $vocabulary->vid] = array(
      '#type' => 'textfield',
      '#title' => t('Custom message for %vocab', array('%vocab' => $vocabulary->name)),
      '#size' => 80,
      '#maxlength' => 255,
      '#default_value' => variable_get('taxiselect_message_not_found_'. $vocabulary->vid, ''),
    );
    $form['multiple_matches']['taxiselect_message_multiple_matches_'. $vocabulary->vid] = array(
      '#type' => 'textfield',
      '#title' => t('Custom message for %vocab', array('%vocab' => $vocabulary->name)),
      '#size' => 80,
      '#maxlength' => 255,
      '#default_value' => variable_get('taxiselect_message_multiple_matches_'. $vocabulary->vid, ''),
    );
  }
  return system_settings_form($form);
}

/**
 * Private helper function to ensure consistent default messages.
 *
 * @param $type
 * @return $string
 */
function _taxiselect_message($type = 'multiple') {
  return $type == 'multiple' ? t('Multiple matches were found for the specified term, where only one match is allowed.') : t('The term could not be found.');
}

/**
 * Core functionality for the ajax response.
 *
 * @param $vid The vocab vid
 * @param $string The string to search for
 * @return none
 */
function taxiselect_autocomplete($vid, $string = '') {
  $results = array();
  foreach (taxiselect_suggestions($vid, $string, variable_get('taxiselect_response_limit', 10)) as $match) {
    $results[implode(', ', $match)] =  implode(" &raquo; ", $match);
  }
  print drupal_to_js($results);
  exit;
}

/**
 * Core functionality for the ajax response.
 *
 * @param $vid The vocab vid
 * @param $string The string to search for
 * @return none
 */
function taxiselect_suggestions($vid, $string, $limit) {
  $matches = array();
  if (drupal_strlen($string) >= variable_get('taxiselect_trigger_length', 3)) {
    $search_terms = explode(',', $string);
    $search_terms = array_filter(array_map('trim', $search_terms));
    // This gives fuzzy matches
    $items =  taxiselect_match_exact_path($search_terms, $vid, $limit - count($matches));
    foreach ($items as $item) {
      $matches[key($item)] = $item;
    }

  }
  uasort($matches, 'taxiselect_sort');
  return $matches;
}

function taxiselect_sort($a, $b) {
  $_a = current($a);
  $_b = current($b);
  do {
    if ($cmp = strcasecmp($_a, $_b)) {
      return $cmp;
    }
    $_a = next($a);
    $_b = next($b);
  }
  while ($_a && $_b);
  if ($_a && !$_b) {
    return 1;
  }
  if (!$_a && $_b) {
    return -1;
  }
  return 0;
}

/**
 * This checks the strict heirachical path, with a fuzzy match.
 * Missing a child - parent relationship will cause this to fail.
 *
 * @param $search_terms An array of term names
 * @param $vid The vocab vid (speeds querries, but optional)
 * @param $limit The number results to return
 * @param $use_wildcard Use the % wildcard. Eg: x like 's%' or x = 's';
 *
 * @return array All located matches up to the limit
 */
function taxiselect_match_exact_path($search_terms, $vid = NULL, $limit = 10, $use_wildcard = TRUE, $validate = FALSE) {
  $items = array();
  $fields = array();
  $conditions = array();
  $joins = array();
  $args = array();
  $index = 0;
  foreach ($search_terms as $index => $search_term) {
    $fields[] = "t{$index}.tid AS tid{$index}";
    $fields[] = "t{$index}.name AS name{$index}";
    if ($vid) {
      $conditions[] = "t{$index}.vid = %d";
      $args[] = $vid;
    }
    if ($use_wildcard) {
      $conditions[] = "(LOWER(t{$index}.name) LIKE LOWER('%s%%') OR LOWER(t{$index}.name) LIKE LOWER('%% %s%%'))";
      $args[] = $search_term;
      $args[] = $search_term;
    }
    else {
      $conditions[] = "LOWER(t{$index}.name) = LOWER('%s')";
      $args[] = $search_term;
    }
    if ($index) {
      $last_index = $index - 1;
      $joins[] = "INNER JOIN  {term_hierarchy} h{$last_index} ON t{$last_index}.tid = h{$last_index}.tid";
      $joins[] = "INNER JOIN  {term_data} t{$index} ON t{$index}.tid = h{$last_index}.parent";
    }
  }
  // restrict to deep of at least 1
  if (variable_get('taxiselect_skip_root', 0)) {
    $joins[] = "INNER JOIN  {term_hierarchy} h{$index} ON t{$index}.tid = h{$index}.tid";
    $conditions[] = "h{$index}.parent != 0";
  }

  $fields = implode(', ', $fields);
  $joins = implode(' ', $joins);
  $conditions = implode(' AND ', $conditions);
  $result = db_query_range("
      SELECT    {$fields}
      FROM      {term_data} t0 {$joins}
      WHERE     {$conditions}", $args, 0, $limit);

  while ($rset = db_fetch_array($result)) {
    // recreate located path, and record the last tid (to create parental path)
    $item = array();
    $pid = 0;
    $counter = 1;
    foreach ($rset as $index => $data) {
      if ($counter % 2) {
        $pid = $data;
      }
      else {
        $item[$pid] = check_plain($data);
      }
      $counter++;
    }
    $parents = taxonomy_get_parents_all($pid);
    if (count($parents) > 1) {
      // current term included
      array_shift($parents);

      // We do not want the root node
      if (variable_get('taxiselect_skip_root', 0)) {
        array_pop($parents);
      }

      // if it has parents then it is not a complete match (for validation)
      if ($validate && count($parents) > 1) {
        continue;
      }
      foreach ($parents as $parent) {
        $item[$parent->tid] = check_plain($parent->name);
      }
    }
    $items[] = $item;
  }
  return $items;
}

/**
 * For any term, generate the full parent path.
 *
 * @example Simple usage
 *
 * $path = '';
 * $parents = taxiselect_parent_path($tid);
 * # Check that the $tid was valid
 * if (count($parents)) {
 *    $path = implode(' &raquo; ', $parents);
 * }
 *
 * @param $tid Term tid
 * @return Parental info about the term
 */
function taxiselect_parent_path($tid) {
  $path = array();
  if ($term = taxonomy_get_term($tid)) {
    $path = array($tid => $term->name);
    do {
      $placeholders = implode(',', array_fill(0, count($path), '%d'));
      $args = $path;
      array_unshift($args, $tid);
      $parent = db_fetch_object(db_query("
          SELECT        h.parent AS tid, p.name
          FROM          {term_data} d
            INNER JOIN  {term_hierarchy} h ON d.tid = h.tid
            INNER JOIN  {term_data} p ON p.tid = h.parent
          WHERE         d.tid = %d
            AND         h.parent NOT IN ({$placeholders})
          LIMIT 1", $tid));
      if ($parent) {
        $path[$parent->tid] = $parent->name;
        $tid = $parent->tid;
      }
    }
    while ($parent);
  }
  if (variable_get('taxiselect_skip_root', 0)) {
    array_pop($path);
  }
  return $path;
}
